FROM "node:8"

LABEL maintainer="raelix@hotmail.it"

RUN [ "npm", "install", "-g", "homebridge", "--unsafe-perm" ]

RUN ["npm","install","-g","homebridge-webos-tv"]

CMD [ "homebridge", "-D" ]
